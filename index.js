console.log(`Hello Feb!`);


// document refers to the whole page
// querrySelector - use to select a specific object(HTML element ) from the document
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// Event Listener

txtFirstName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

txtFirstName.addEventListener("keyup", (event) => {
	console.log(event.target);
	console.log(event.target.value);
})

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);

function updateFullName() {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}


